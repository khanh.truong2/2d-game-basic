using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapSpikedBall : Trap
{
    private Rigidbody2D rb;
    [SerializeField] private Vector2 pushDirection;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.AddForce(pushDirection, ForceMode2D.Impulse);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
