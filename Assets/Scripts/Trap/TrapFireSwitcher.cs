using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapFireSwitcher : MonoBehaviour
{
    public TrapFire myTrap;
    private Animator anim;

    public float timeNotActive = 2;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {

    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.GetComponent<PlayerMovement>() != null)
        {
            anim.SetTrigger("pressed");
            myTrap.FireSwitchAfter(timeNotActive);
        }
    }
}
