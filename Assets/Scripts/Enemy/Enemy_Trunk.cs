using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Trunk : Enemy
{
    [Header("Trunk Specific")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletOrigin;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private float attackCooldown;
    private float attackCooldownCounter;

    protected override void Start()
    {
        base.Start();
    }

    void Update()
    {
        CollisionChecks();

        if (!canMove)
        {
            rb.velocity = Vector2.zero;
        }

        attackCooldownCounter -= Time.deltaTime;

        if (playerDitection.collider.GetComponent<PlayerMovement>() != null)
        {
            if (attackCooldownCounter < 0)
            {
                attackCooldownCounter = attackCooldown;
                anim.SetTrigger("attack");
                canMove = false;
            }
        }
        else
        {
            canMove = true;
            WalkAround();
        }

        anim.SetFloat("xVelocity", rb.velocity.x);
    }

    private void AttackEvent()
    {
        GameObject newBullet = Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.rotation);

        newBullet.GetComponent<Bullet>().SetupSpeed(bulletSpeed * facingDirection, 0);

        Destroy(newBullet, 3f);
    }
}
