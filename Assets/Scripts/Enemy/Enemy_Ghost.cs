using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Ghost : Enemy
{
    [Header("Ghost specific")]
    [SerializeField] private float activeTime;
    private float activeTimeCounter = 4;

    private Transform player;
    private SpriteRenderer sr;

    [SerializeField] private float[] xOffset;

    protected override void Start()
    {
        base.Start();

        sr = GetComponent<SpriteRenderer>();
        aggressive = true;
        invincible = true;

        player = GameObject.Find("Player").transform;
    }

    void Update()
    {
        activeTimeCounter -= Time.deltaTime;
        idleTimeCounter -= Time.deltaTime;

        if (activeTimeCounter > 0)
        {
            transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
        }

        if (activeTimeCounter < 0 && idleTimeCounter < 0 && aggressive)
        {
            anim.SetTrigger("disappear");
            idleTimeCounter = idleTime;
            aggressive = false;
        }

        if (activeTimeCounter < 0 && idleTimeCounter < 0 && !aggressive)
        {
            ChoosePosition();
            anim.SetTrigger("appear");
            activeTimeCounter = activeTime;
            aggressive = true;
        }

        if (facingDirection == -1 && transform.position.x < player.transform.position.x)
        {
            Flip();
        }
        else if (facingDirection == 1 && transform.position.x > player.transform.position.x)
        {
            Flip();
        }
    }

    private void ChoosePosition ()
    {
        float _xOffet = xOffset[Random.Range(0, xOffset.Length)];
        float _yOffet = Random.Range(-10, 10);
        transform.position = new Vector2(player.transform.position.x + _xOffet, player.transform.position.y + _yOffet);
    }

    public void Disappear()
    {
        //sr.enabled = false;
        sr.color = Color.clear;
    }

    public void Appear()
    {
        //sr.enabled = true;
        sr.color = Color.white;
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (aggressive)
            base.OnTriggerEnter2D(collision);
    }
}
