using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Plant : Enemy
{
    [Header("Plant Specific")]
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform bulletOrigin;
    [SerializeField] private float bulletSpeed;
    [SerializeField] private bool facingRight;

    protected override void Start()
    {
        base.Start();

        if (facingRight)
        {
            Flip();
        }
    }

    void Update()
    {
        CollisionChecks();

        idleTimeCounter -= Time.deltaTime;

        bool playerDetected = playerDitection.collider.GetComponent<PlayerMovement>() != null;
        if (idleTimeCounter < 0 && playerDetected)
        {
            idleTimeCounter = idleTime;
            anim.SetTrigger("attack");
        }
    }

    private void AttackEvent()
    {
        GameObject newBullet = Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.rotation);

        newBullet.GetComponent<Bullet>().SetupSpeed(bulletSpeed * facingDirection, 0);

        Destroy(newBullet, 3f);
    }
}
