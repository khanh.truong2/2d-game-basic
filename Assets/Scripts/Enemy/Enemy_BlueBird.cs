using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_BlueBird : Enemy
{
    private RaycastHit2D ceillingDetected;

    [Header("Blue Bird Specific")]
    [SerializeField] private float ceillingDistance;
    [SerializeField] private float groundDistance;


    [SerializeField] private float flyUpForce;
    [SerializeField] private float flyDownForce;
    private float flyForce;

    private bool canFly = true;

    protected override void Start()
    {
        base.Start();

        flyForce = flyUpForce;
    }

    void Update()
    {
        CollisionChecks();
        if (ceillingDetected)
        {
            flyForce = flyDownForce;
        }
        else if (groundDetected)
        {
            flyForce = flyUpForce;
        }

        if (wallDetected)
        {
            Flip();
        }
    }

    public void FlyUpEvent()
    {
        if (canFly)
            rb.velocity = new Vector2(speed * facingDirection, flyForce);
    }

    public override void Damage()
    {
        canFly = false;
        rb.velocity = Vector2.zero;
        rb.gravityScale = 0;
        base.Damage();
    }

    protected override void CollisionChecks()
    {
        base.CollisionChecks();

        ceillingDetected = Physics2D.Raycast(transform.position, Vector2.up, ceillingDistance, whatIsGround);
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y + ceillingDistance));
        Gizmos.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y - groundDistance));
    }
}
