using UnityEngine;

public class Enemy_Rino : Enemy
{
    [Header("Rino Specific")]
    [SerializeField] private float aggressiveSpeed;
    [SerializeField] private float shockTime = 1;
    private float shockTimeCounter;

    protected override void Start()
    {
        base.Start();
        invincible = true;
    }

    // Update is called once per frame
    void Update()
    {
        CollisionChecks();

        if (playerDitection.collider.GetComponent<PlayerMovement>() != null) aggressive = true;

        if (!aggressive)
        {
            WalkAround();
        }
        else
        {
            if (!groundDetected)
            {
                aggressive = false;
                Flip();
            }

            rb.velocity = new Vector2(aggressiveSpeed * facingDirection, rb.velocity.y);

            if (wallDetected && invincible)
            {
                invincible = false;
                shockTimeCounter = shockTime;
            }

            if (shockTimeCounter > 0)
            {
                rb.velocity = Vector2.zero;
            }

            shockTimeCounter -= Time.deltaTime;

            if (shockTimeCounter <= 0 && !invincible)
            {
                invincible = true;
                Flip();
                aggressive = false;
            }
        }

        AnimatorController();
    }

    private void AnimatorController()
    {
        anim.SetBool("invincible", invincible);
        anim.SetFloat("xVelocity", rb.velocity.x);
    }
}
