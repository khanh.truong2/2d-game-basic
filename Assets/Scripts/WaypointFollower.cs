using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointFollower : Trap
{
    [SerializeField] private GameObject[] waypoints;
    private int currentWaypointIndex = 0;

    [SerializeField] private float speed = 3f;

    private void Update()
    {
        if (Vector2.Distance(waypoints[currentWaypointIndex].transform.position, transform.position) < .1f)
        {
            currentWaypointIndex++;
            if(currentWaypointIndex >= waypoints.Length)
            {
                currentWaypointIndex = 0;
            }
        }
        transform.position = Vector2.MoveTowards(transform.position, waypoints[currentWaypointIndex].transform.position, Time.deltaTime * speed);
    }

    //protected override void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (isWorking)
    //    {
    //        base.OnTriggerEnter2D(collision);
    //    }
    //    else
    //    {
    //        Debug.Log("not Working");
    //    }
    //}
}
