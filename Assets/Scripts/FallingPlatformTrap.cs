using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FallingPlatformTrap : MonoBehaviour
{
    [SerializeField] private float fallDeplay = .4f;

    [SerializeField] private Rigidbody2D rb;

    private IEnumerator Fall()
    {
        yield return new WaitForSeconds(fallDeplay);
        gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            StartCoroutine(Fall());
        }
    }
}
