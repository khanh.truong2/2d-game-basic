using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject myCamera;

    private void Start()
    {
        //myCamera.GetComponent<CinemachineVirtualCamera>().Follow = PlayerManager.instance.currentPlayer.transform;
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMovement>() != null)
        {
            myCamera.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.GetComponent<PlayerMovement>() != null)
        {
            myCamera.SetActive(true);
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = gizmosColor;
    //    Gizmos.DrawWireCube(cd.bounds.center, cd.bounds.size);
    //}
}
