using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator anim;

    [SerializeField] private AudioSource deathSoundEffect;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Trap") || collision.gameObject.CompareTag("FakeCherries"))
        {
            Debug.Log("OnCollision");

            deathSoundEffect.Play();
            Die();
        }

        if (collision.gameObject.CompareTag("HiddenTrap"))
        {
            collision.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
            deathSoundEffect.Play();
            Die();
        }

        if (collision.gameObject.CompareTag("HiddenTerrain"))
        {
            collision.gameObject.GetComponent<SpriteRenderer>().color = new Color(1.0f, 1.0f, 1.0f, 1.0f);
        }
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("FallDetector") || collision.gameObject.CompareTag("ArrowTrap"))
        {
            deathSoundEffect.Play();
            Die();
        }

        if (collision.gameObject.CompareTag("Trap"))
        {
            Debug.Log("Ontrigger");
            deathSoundEffect.Play();
            Die();
        }
    }

    private void Die()
    {
        rb.bodyType = RigidbodyType2D.Static;
        anim.SetTrigger("death");
    }

    private void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
