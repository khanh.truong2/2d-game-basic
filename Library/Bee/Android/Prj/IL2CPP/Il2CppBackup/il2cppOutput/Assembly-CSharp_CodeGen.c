﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CallVibrator::Vibrate()
extern void CallVibrator_Vibrate_m8392491F1E93039F5DCF1FA0593B698D5D8FF46B (void);
// 0x00000002 System.Void CallVibrator::.ctor()
extern void CallVibrator__ctor_m56A7305814E3DD6D047B7932993E5A0F8F4284BC (void);
// 0x00000003 System.Void CameraController::Update()
extern void CameraController_Update_mBCB871B23DBA60444D43AB56D780478BA3D355E6 (void);
// 0x00000004 System.Void CameraController::.ctor()
extern void CameraController__ctor_mE196A6332BDDED632D6F9DB6260E424594598950 (void);
// 0x00000005 System.Void EndMenu::Quit()
extern void EndMenu_Quit_mBE66D39C6448A0681E74E25F3F4C508E531A77E3 (void);
// 0x00000006 System.Void EndMenu::.ctor()
extern void EndMenu__ctor_m4C7779DFDF13E88029D6B0E1930B432A3CD5C0FA (void);
// 0x00000007 System.Void Finish::Start()
extern void Finish_Start_mA9AFFF54C6C4074C51DB4A9C91DE3F7F91C48433 (void);
// 0x00000008 System.Void Finish::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Finish_OnTriggerEnter2D_mAF84A22C805B5AAEB35D9EC57EA28D44CA8226C8 (void);
// 0x00000009 System.Void Finish::CompleteLevel()
extern void Finish_CompleteLevel_m885320627617C80B183BE415E1E5D7CE79E61533 (void);
// 0x0000000A System.Void Finish::.ctor()
extern void Finish__ctor_m1C22870293D937B3091D24E0C0EB27B7DF08B48B (void);
// 0x0000000B System.Void HiddenTerrain::Start()
extern void HiddenTerrain_Start_mCF076368B39AD5D2BDCC9A7974BB9804C626BB11 (void);
// 0x0000000C System.Void HiddenTerrain::Update()
extern void HiddenTerrain_Update_m2E858E3F03DE179204E712396717DC0BA2805E97 (void);
// 0x0000000D System.Void HiddenTerrain::.ctor()
extern void HiddenTerrain__ctor_mE9C25010007477DD7D39A4B6764D40F9FAF16ABD (void);
// 0x0000000E System.Void ItemCollector::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ItemCollector_OnTriggerEnter2D_m301956EFACC34CAF2B2210F7A056F2CE1C4EED78 (void);
// 0x0000000F System.Int32 ItemCollector::countCherries()
extern void ItemCollector_countCherries_m1EFAE0E25A6805618616CEFAF086E85074DD9B87 (void);
// 0x00000010 System.Void ItemCollector::.ctor()
extern void ItemCollector__ctor_m96F656A12F62C94C429420D7AE3FBEF52A61BE49 (void);
// 0x00000011 System.Void PlayerLife::Start()
extern void PlayerLife_Start_m47CC209164DA35A2C1ECB2A472E3C3471792D7B6 (void);
// 0x00000012 System.Void PlayerLife::OnCollisionEnter2D(UnityEngine.Collision2D)
extern void PlayerLife_OnCollisionEnter2D_mA4388433B33F72F394083146AECAAFF02D694FC9 (void);
// 0x00000013 System.Void PlayerLife::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void PlayerLife_OnTriggerEnter2D_m7F5E0CDD3A820A7FB0C583A51C90C38F31B35125 (void);
// 0x00000014 System.Void PlayerLife::Die()
extern void PlayerLife_Die_m27F738218CB6CC3CA30DEC6118490521BA393ABD (void);
// 0x00000015 System.Void PlayerLife::RestartLevel()
extern void PlayerLife_RestartLevel_mA0A2C4D3A942C0611C7AC70D57F6E8A74F839FA1 (void);
// 0x00000016 System.Void PlayerLife::.ctor()
extern void PlayerLife__ctor_m15B9ED6A5AE67DFABC0038BFDABC559B6CCACF37 (void);
// 0x00000017 System.Void PlayerMovement::Start()
extern void PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A (void);
// 0x00000018 System.Void PlayerMovement::Update()
extern void PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3 (void);
// 0x00000019 System.Void PlayerMovement::Jump()
extern void PlayerMovement_Jump_mCA26EA1D2892C47F04966998153F3FD4FFBCEB43 (void);
// 0x0000001A System.Void PlayerMovement::UpdateAnimationUpdate()
extern void PlayerMovement_UpdateAnimationUpdate_mC91353A8C696635A0F050215D7EBB26885FE4E38 (void);
// 0x0000001B System.Boolean PlayerMovement::IsGrounded()
extern void PlayerMovement_IsGrounded_mE94AF8C5B07720B09A63074AE38DF0CF67BEBDD6 (void);
// 0x0000001C System.Void PlayerMovement::.ctor()
extern void PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA (void);
// 0x0000001D System.Void Rotate::Update()
extern void Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7 (void);
// 0x0000001E System.Void Rotate::.ctor()
extern void Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4 (void);
// 0x0000001F System.Void StartMenu::StartGame()
extern void StartMenu_StartGame_m322304C7FCCF49E44379A0586A424E138823FB86 (void);
// 0x00000020 System.Void StartMenu::.ctor()
extern void StartMenu__ctor_mF4B8FB56F43BCB65755F6B81F1C9446FEE79C577 (void);
// 0x00000021 System.Void StickyPlatform::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void StickyPlatform_OnTriggerEnter2D_mE5B71936CA710F38D6DECA19AEF7A11666EC88AB (void);
// 0x00000022 System.Void StickyPlatform::OnTriggerExit2D(UnityEngine.Collider2D)
extern void StickyPlatform_OnTriggerExit2D_mF1A09F85A5013D6FDC8358CCD7AE90549E49CB47 (void);
// 0x00000023 System.Void StickyPlatform::.ctor()
extern void StickyPlatform__ctor_m437CC7AF465F618BD16D05E0CAFEAAA2B5E9C05F (void);
// 0x00000024 System.Void UIManager::PauseGame(System.Boolean)
extern void UIManager_PauseGame_m527DC8B004E667E7CA7F4B7ABD09C8C41C05D144 (void);
// 0x00000025 System.Void UIManager::Start()
extern void UIManager_Start_m113F392674AB08A26877728CD36F06332E869080 (void);
// 0x00000026 System.Void UIManager::Update()
extern void UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB (void);
// 0x00000027 System.Void UIManager::MainMenu()
extern void UIManager_MainMenu_mE0EC0435110412FCF884C29F0323F4E2C6F6B427 (void);
// 0x00000028 System.Void UIManager::Quit()
extern void UIManager_Quit_m2D595569EA701AF260F77E48259176531FEE66A8 (void);
// 0x00000029 System.Void UIManager::.ctor()
extern void UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F (void);
// 0x0000002A System.Void Vibrator::Vibrate(System.Int64)
extern void Vibrator_Vibrate_m01CF094D9646B019AC5230311F196E52D8ADA144 (void);
// 0x0000002B System.Void Vibrator::Cancel()
extern void Vibrator_Cancel_m15FCFFDCB4999DB55D9750FC914696FA8D5142FA (void);
// 0x0000002C System.Boolean Vibrator::IsAndroid()
extern void Vibrator_IsAndroid_mFDDBEBC80AC054E89483F2E4C0D5E110EC3CB942 (void);
// 0x0000002D System.Void Vibrator::.cctor()
extern void Vibrator__cctor_mFC889C466FF16A203C052562DC8462CED4A69788 (void);
// 0x0000002E System.Void WaypointFollower::Update()
extern void WaypointFollower_Update_m2B0F78C965A8D9B2C0FB01E88B7A0DE3A885AB94 (void);
// 0x0000002F System.Void WaypointFollower::.ctor()
extern void WaypointFollower__ctor_mF34A650189CA4EE5F7C93AC5387854B89A6DA274 (void);
// 0x00000030 UnityEngine.InputSystem.InputActionAsset GameInput.InputActions::get_asset()
extern void InputActions_get_asset_m0B404D3A61CA14A18187AC701AB77156054E9876 (void);
// 0x00000031 System.Void GameInput.InputActions::.ctor()
extern void InputActions__ctor_mAC3FDD94A25550D125BA9F3AC43508E3FB1F2167 (void);
// 0x00000032 System.Void GameInput.InputActions::Dispose()
extern void InputActions_Dispose_mDB516670F50F9B246B7647EB3DBA57800F59D95D (void);
// 0x00000033 System.Nullable`1<UnityEngine.InputSystem.InputBinding> GameInput.InputActions::get_bindingMask()
extern void InputActions_get_bindingMask_mB6B4F85FF2F38AFF37DA1C7AE42B8D28FEAED698 (void);
// 0x00000034 System.Void GameInput.InputActions::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void InputActions_set_bindingMask_m68404A8215E3F86D161EBC87F8F9386239ACD97D (void);
// 0x00000035 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> GameInput.InputActions::get_devices()
extern void InputActions_get_devices_m0DA81920D100A3A78F8AA81E657E8DDC788B02E5 (void);
// 0x00000036 System.Void GameInput.InputActions::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void InputActions_set_devices_mA5F20C0563297EDAB1C399A5726D5FE7EB55B971 (void);
// 0x00000037 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> GameInput.InputActions::get_controlSchemes()
extern void InputActions_get_controlSchemes_mA52697C051F66F823D14FBF7CB1D04A75DAA4C33 (void);
// 0x00000038 System.Boolean GameInput.InputActions::Contains(UnityEngine.InputSystem.InputAction)
extern void InputActions_Contains_m5FA6C9B10ADD4E910C7BDB80613095BAA499C248 (void);
// 0x00000039 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> GameInput.InputActions::GetEnumerator()
extern void InputActions_GetEnumerator_m374302E8753C0FCA7D92DD05ABA50E74BF50F792 (void);
// 0x0000003A System.Collections.IEnumerator GameInput.InputActions::System.Collections.IEnumerable.GetEnumerator()
extern void InputActions_System_Collections_IEnumerable_GetEnumerator_m70A24FCA6CF31C2D8C75D256576FB8878CED153A (void);
// 0x0000003B System.Void GameInput.InputActions::Enable()
extern void InputActions_Enable_m7B21A764A76A4B282377170E7AF1CED1D6A5D9CB (void);
// 0x0000003C System.Void GameInput.InputActions::Disable()
extern void InputActions_Disable_m6A37A4FD43705269541E865B580596EAE28139DE (void);
// 0x0000003D System.Collections.Generic.IEnumerable`1<UnityEngine.InputSystem.InputBinding> GameInput.InputActions::get_bindings()
extern void InputActions_get_bindings_m0F3CF198399EFDDE589B936BA80DBD68EC35E8F5 (void);
// 0x0000003E UnityEngine.InputSystem.InputAction GameInput.InputActions::FindAction(System.String,System.Boolean)
extern void InputActions_FindAction_mFD76059AFDEFD652BC8B82A7DBE76C280DFC98FE (void);
// 0x0000003F System.Int32 GameInput.InputActions::FindBinding(UnityEngine.InputSystem.InputBinding,UnityEngine.InputSystem.InputAction&)
extern void InputActions_FindBinding_m6B7763EABE5A8E54206D3FA722405A8FC6F3ABB0 (void);
static Il2CppMethodPointer s_methodPointers[63] = 
{
	CallVibrator_Vibrate_m8392491F1E93039F5DCF1FA0593B698D5D8FF46B,
	CallVibrator__ctor_m56A7305814E3DD6D047B7932993E5A0F8F4284BC,
	CameraController_Update_mBCB871B23DBA60444D43AB56D780478BA3D355E6,
	CameraController__ctor_mE196A6332BDDED632D6F9DB6260E424594598950,
	EndMenu_Quit_mBE66D39C6448A0681E74E25F3F4C508E531A77E3,
	EndMenu__ctor_m4C7779DFDF13E88029D6B0E1930B432A3CD5C0FA,
	Finish_Start_mA9AFFF54C6C4074C51DB4A9C91DE3F7F91C48433,
	Finish_OnTriggerEnter2D_mAF84A22C805B5AAEB35D9EC57EA28D44CA8226C8,
	Finish_CompleteLevel_m885320627617C80B183BE415E1E5D7CE79E61533,
	Finish__ctor_m1C22870293D937B3091D24E0C0EB27B7DF08B48B,
	HiddenTerrain_Start_mCF076368B39AD5D2BDCC9A7974BB9804C626BB11,
	HiddenTerrain_Update_m2E858E3F03DE179204E712396717DC0BA2805E97,
	HiddenTerrain__ctor_mE9C25010007477DD7D39A4B6764D40F9FAF16ABD,
	ItemCollector_OnTriggerEnter2D_m301956EFACC34CAF2B2210F7A056F2CE1C4EED78,
	ItemCollector_countCherries_m1EFAE0E25A6805618616CEFAF086E85074DD9B87,
	ItemCollector__ctor_m96F656A12F62C94C429420D7AE3FBEF52A61BE49,
	PlayerLife_Start_m47CC209164DA35A2C1ECB2A472E3C3471792D7B6,
	PlayerLife_OnCollisionEnter2D_mA4388433B33F72F394083146AECAAFF02D694FC9,
	PlayerLife_OnTriggerEnter2D_m7F5E0CDD3A820A7FB0C583A51C90C38F31B35125,
	PlayerLife_Die_m27F738218CB6CC3CA30DEC6118490521BA393ABD,
	PlayerLife_RestartLevel_mA0A2C4D3A942C0611C7AC70D57F6E8A74F839FA1,
	PlayerLife__ctor_m15B9ED6A5AE67DFABC0038BFDABC559B6CCACF37,
	PlayerMovement_Start_m83FD44DCA324CE3D05A71FD2E2991FCD743F003A,
	PlayerMovement_Update_m5BB6CE35AF68EE00CFEB4BA5EBA17E10667551D3,
	PlayerMovement_Jump_mCA26EA1D2892C47F04966998153F3FD4FFBCEB43,
	PlayerMovement_UpdateAnimationUpdate_mC91353A8C696635A0F050215D7EBB26885FE4E38,
	PlayerMovement_IsGrounded_mE94AF8C5B07720B09A63074AE38DF0CF67BEBDD6,
	PlayerMovement__ctor_mB37559C5B0638161878D20E00B7C672FC38BBBAA,
	Rotate_Update_m73D585515036D9B7AAD8336BFB8567283CE4C7E7,
	Rotate__ctor_m0EE5CC8EB699542BFC438DC3D547D39E442E9EE4,
	StartMenu_StartGame_m322304C7FCCF49E44379A0586A424E138823FB86,
	StartMenu__ctor_mF4B8FB56F43BCB65755F6B81F1C9446FEE79C577,
	StickyPlatform_OnTriggerEnter2D_mE5B71936CA710F38D6DECA19AEF7A11666EC88AB,
	StickyPlatform_OnTriggerExit2D_mF1A09F85A5013D6FDC8358CCD7AE90549E49CB47,
	StickyPlatform__ctor_m437CC7AF465F618BD16D05E0CAFEAAA2B5E9C05F,
	UIManager_PauseGame_m527DC8B004E667E7CA7F4B7ABD09C8C41C05D144,
	UIManager_Start_m113F392674AB08A26877728CD36F06332E869080,
	UIManager_Update_m95D2E80B8F461F15C1B9BD6DB0811F5CC18571AB,
	UIManager_MainMenu_mE0EC0435110412FCF884C29F0323F4E2C6F6B427,
	UIManager_Quit_m2D595569EA701AF260F77E48259176531FEE66A8,
	UIManager__ctor_mC9DC2B8984E76F424E73C1860AD4BD3DEBF6573F,
	Vibrator_Vibrate_m01CF094D9646B019AC5230311F196E52D8ADA144,
	Vibrator_Cancel_m15FCFFDCB4999DB55D9750FC914696FA8D5142FA,
	Vibrator_IsAndroid_mFDDBEBC80AC054E89483F2E4C0D5E110EC3CB942,
	Vibrator__cctor_mFC889C466FF16A203C052562DC8462CED4A69788,
	WaypointFollower_Update_m2B0F78C965A8D9B2C0FB01E88B7A0DE3A885AB94,
	WaypointFollower__ctor_mF34A650189CA4EE5F7C93AC5387854B89A6DA274,
	InputActions_get_asset_m0B404D3A61CA14A18187AC701AB77156054E9876,
	InputActions__ctor_mAC3FDD94A25550D125BA9F3AC43508E3FB1F2167,
	InputActions_Dispose_mDB516670F50F9B246B7647EB3DBA57800F59D95D,
	InputActions_get_bindingMask_mB6B4F85FF2F38AFF37DA1C7AE42B8D28FEAED698,
	InputActions_set_bindingMask_m68404A8215E3F86D161EBC87F8F9386239ACD97D,
	InputActions_get_devices_m0DA81920D100A3A78F8AA81E657E8DDC788B02E5,
	InputActions_set_devices_mA5F20C0563297EDAB1C399A5726D5FE7EB55B971,
	InputActions_get_controlSchemes_mA52697C051F66F823D14FBF7CB1D04A75DAA4C33,
	InputActions_Contains_m5FA6C9B10ADD4E910C7BDB80613095BAA499C248,
	InputActions_GetEnumerator_m374302E8753C0FCA7D92DD05ABA50E74BF50F792,
	InputActions_System_Collections_IEnumerable_GetEnumerator_m70A24FCA6CF31C2D8C75D256576FB8878CED153A,
	InputActions_Enable_m7B21A764A76A4B282377170E7AF1CED1D6A5D9CB,
	InputActions_Disable_m6A37A4FD43705269541E865B580596EAE28139DE,
	InputActions_get_bindings_m0F3CF198399EFDDE589B936BA80DBD68EC35E8F5,
	InputActions_FindAction_mFD76059AFDEFD652BC8B82A7DBE76C280DFC98FE,
	InputActions_FindBinding_m6B7763EABE5A8E54206D3FA722405A8FC6F3ABB0,
};
static const int32_t s_InvokerIndices[63] = 
{
	4253,
	4253,
	4253,
	4253,
	4253,
	4253,
	4253,
	3398,
	4253,
	4253,
	4253,
	4253,
	4253,
	3398,
	4138,
	4253,
	4253,
	3398,
	3398,
	4253,
	4253,
	4253,
	4253,
	4253,
	4253,
	4253,
	4084,
	4253,
	4253,
	4253,
	4253,
	4253,
	3398,
	3398,
	4253,
	3323,
	4253,
	4253,
	4253,
	4253,
	4253,
	6451,
	6619,
	6553,
	6619,
	4253,
	4253,
	4161,
	4253,
	4253,
	4006,
	3257,
	4004,
	3255,
	4015,
	2381,
	4161,
	4161,
	4253,
	4253,
	4161,
	1449,
	1274,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	63,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
